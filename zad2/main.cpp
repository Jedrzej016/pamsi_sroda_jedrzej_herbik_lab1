#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>

using namespace std;

int DLUGOSC=1;
int *tablica;

void wyswietl_tablice();
void wypelnij_tablice_l_losowymi();
void wypelnij_tablice_z_terminalu();
int odczyt_z_txt();
void zapis_do_txt();
int odczyt_z_bin();
void zapis_do_bin();

int main(int argc, char *argv[]){ /*glowna funkcja zawierajaca menu wyboru*/
  char opcja=0;
  cout<<endl<<"Podaj dlugosc tablicy: ";
  cin>>DLUGOSC;
  tablica=new int [DLUGOSC]; // stworzenie tablicy o zadanej dlugosci
  do{
    cout << "1.Wczytanie tablicy z konsoli"<<endl;
    cout << "2.Wyświetlenie tablicy"<<endl;
    cout << "3.Wczytywanie z pliku tekstowego"<<endl;
    cout << "4.Zapisywanie do pliku tekstowego"<<endl;
    cout << "5.Wczytywanie z pliku binarnego"<<endl;
    cout << "6.Zapisywanie do pliku binarnego"<<endl;
    cout << "7.Wypełnianie tablicy losowymi wartosciami"<<endl;
    cout << "0.Koniec"<<endl;
    cout << "Wybierz opcje: ";
    cin >> opcja;
    switch(opcja)
      {
      case '0':
	break; // zmienna ocpja przypiera wartosc '0' i konczy f. do...while a break konczy f. switch
      case '1':
	wypelnij_tablice_z_terminalu();
	break;
      case '2':
	wyswietl_tablice();
	break;
      case '3':
	odczyt_z_txt();
	break;
      case '4':
	zapis_do_txt();
	break;
      case '5':
	odczyt_z_bin();
	break;
      case '6':
	zapis_do_bin();
	break;
      case '7':
	wypelnij_tablice_l_losowymi();
	break;
      default:
	cout<<"Zle okreslony wybor"<<endl;
	break;
      }
  }while(opcja !='0');
  return 0;
}


void wypelnij_tablice_l_losowymi(){
  cout<<endl;
  srand(time(NULL)); // ustalenie punktu poczatkowego w danym monencie czasu procesora
  for(int dl=0;dl<DLUGOSC;dl++){
    tablica[dl]=std::rand()%10; // przypisywanie losowej wartosci do zmiennej
  }
  cout<<endl;
}

void wyswietl_tablice(){
  cout<<endl;
  for(int dl=0;dl<DLUGOSC;dl++){
    cout<<tablica[dl]<<" "; // wyswietlanie elementow
  }
  cout<<endl<<endl;
}

void wypelnij_tablice_z_terminalu(){
  cout<<endl;
  for(int dl=0;dl<DLUGOSC;dl++){
    cout<<"Podaj wartosc elementu["<<dl<<"]: ";
    cin>>tablica[dl];
  }
  cout<<endl;
}

int odczyt_z_txt(){
  ifstream plik_wej;
  cout<<endl;
  plik_wej.open( "test.txt");
  if(!( plik_wej.good() == true )){ // sprawdzenie poprawnosci podanego pliku
    cout<<"plik niepoprawny"<<endl;
    plik_wej.close(); // zamkniecie pliku
    cout<<endl;
    return 1; // zakonczenie funckji w przypadku blednego pliku
  }
  for(int i=0;i<DLUGOSC;i++)plik_wej>>tablica[i];  // odczyt danych
  plik_wej.close(); // zamkniecie pliku
  cout<<endl;
  return 0;
}

void zapis_do_txt(){
  cout<<endl;
  ofstream plik_wyj("test.txt");
  for(int i=0;i<DLUGOSC;i++)plik_wyj<<tablica[i]<<" "; // zapis danych
  plik_wyj.close(); // zamkniecie pliku
  cout<<endl;
}

int odczyt_z_bin(){
  ifstream plik_wej;
  cout<<endl;
  plik_wej.open("test.bin");
  if(!( plik_wej.good() == true )){ // sprawdzenie poprawnosci podanego pliku
    cout<<"plik niepoprawny"<<endl;
    plik_wej.close(); // zamkniecie pliku
    cout<<endl;
    return 1; // zakonczenie funckji w przypadku blednego pliku
  }
  for(int i=0;i<DLUGOSC;i++)plik_wej.read(( char * ) & tablica[i], sizeof tablica[i] ); // odczyt danych
  plik_wej.close(); // zamkniecie pliku
  cout<<endl;
  return 0;
}

void zapis_do_bin(){
  ofstream plik_wyj("test.bin", std::ios::binary );
  for(int i=0;i<DLUGOSC;i++)plik_wyj.write(( const char * ) & tablica[i], sizeof tablica[i] ); // zapis danych
  plik_wyj.close(); // zamkniecie pliku
  cout<<endl;
}
