#include <iostream>

using namespace std;

int Potega(int x, int p);
int Silnia(int x);

int main(int argc, char *argv[]){ /*glowna funkcja zawierajaca menu wyboru*/
  int liczba,potega;
  char wybor=0; // zmienna do odblugi menu
  while(1){ // proste menu do testowania funkcji
    cout<<endl<<"1.Potega"<<endl<<"2.Silnia"<<endl<<"q.Wyjscie"<<endl<<"Wybor: ";
    cin>>wybor;
    if(wybor=='1'){ // wybor potegi
      cout<<"Podaj wartosc podstawy: ";
      cin>>liczba;
      cout<<"Podaj wartosc potegi: ";
      cin>>potega;
      cout<<endl<<"Wynik to: "<<Potega(liczba,potega)<<endl;
    }else if(wybor=='2'){ // wybor silni
      cout<<"Podaj liczbe: ";
      cin>>liczba;
      cout<<endl<<"Wynik to: "<<Silnia(liczba)<<endl;
    }else if(wybor=='q'){ // wybor zamkniecia programu
      return 0;
    }else cout<<endl<<"Zle okreslony wybor"<<endl;
  }
}

int Potega(int x, int p){
  if(!p)return 1;
  else return x*Potega(x,p-1);
}

int Silnia(int x){
  if(!x)return 1;
  else return x*Silnia(x-1);
}
