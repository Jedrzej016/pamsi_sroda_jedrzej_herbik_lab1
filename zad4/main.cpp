#include<iostream>
#include<string>

using namespace std;

bool jestPal(string testStr);

int main(int argc, char *argv[]){
  string wyraz;
  while(1){
    cout<<endl<<"Podaj slowo: ";
    cin>>wyraz;
    if(jestPal(wyraz))cout<<endl<<"Slowo "<<wyraz<<" jest palindromem."<<endl; // slowo jest palindromem
    else cout<<endl<<"Slowo "<<wyraz<<" nie jest palindromem."<<endl; // slowo nie jest palindromem
  }
  return 0;
}

bool jestPal(string testStr){
  int dl=testStr.length();
  if(dl==1)return true; // ciag o jednym znaku jest palindromem
  else if(dl==2){       // sprawdzenie siagu dwuznakowych
      if((testStr[0]==testStr[1])||(testStr[0]==testStr[1]+32)||(testStr[0]==testStr[1]-32))return true;
      else return false;
    }
  if(!((testStr[0]==testStr[dl-1])||(testStr[0]==testStr[dl-1]+32)||(testStr[0]==testStr[dl-1]-32)))return false; // sprawdzenie elementow skrajnych
  if(!(jestPal(testStr.substr(1,dl-2))))return false;  // wywolanie rekurencyjne
  return true;
  // funkcja zwraca true, nie tylko gdy wszystkie litery lezace w rownej ogleglosci od srodka slowa sa rowne (np. 'a' oraz 'a'),
  // funkcja uznaje rowniez za identyczne litery male i duze (np. 'A' oraz 'a'), dlatego jest tak rozbudowany warunek funkcji if 
}
