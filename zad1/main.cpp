#include<iostream>
#include<cstdlib>
#include<ctime>

int **tablica;
int kolumny=0;
int wiersze=0;

using namespace std;

void wyswietl_tablice();
void wypelnij_tablice_z_terminalu();
void wypelnij_tablice_l_losowymi();
int wartosc_max_tablicy();

int main(int argc, char *argv[]){ /*glowna funkcja zawierajaca menu wyboru*/
  char opcja=0; // zmienna sterująca menu
  cout<<endl<<"Podaj lilosc kolumn: ";
  cin>>kolumny;
  tablica=new int* [kolumny]; // stworzenie tablicy wskaznikow o dlugosci wiersza tablicy
  cout<<endl<<"Podaj lilosc wierszy: ";
  cin>>wiersze;
  cout<<endl;
  for(int i=0;i<kolumny;i++){
    tablica[i]=new int [wiersze]; // tworzenie kolumn z tablicy wskaznikow
  }
  do{
    cout << "1.Wczytanie tablicy z konsoli"<<endl;
    cout << "2.Wyswietlenie tablicy"<<endl;
    cout << "3.Wypelnianie tablicy losowymi liczbami"<<endl;
    cout << "4.Szukanie wartości maksymalnej"<<endl;
    cout << "0.Koniec"<<endl;
    cout << "Wybierz opcje: ";
    cin >> opcja;
    switch(opcja){
      case '0':
	break;
      case '1':
	wypelnij_tablice_z_terminalu();
	break;
      case '2':
	wyswietl_tablice();
	break;
      case '3':
	wypelnij_tablice_l_losowymi();
	break;
      case '4':
	cout<<endl<<"Maksymalna wartosc tablicy to: "<<wartosc_max_tablicy()<<endl<<endl;	
	break;
      default:
	cout<<"Zle okreslony wybor"<<endl;
	break;
    }
  }while(opcja !='0');
  for(int i=0;i<kolumny;i++){
    delete [] tablica[i]; // zwalnianie pamieci z wierszy tablicy
  }
  delete [] tablica; // zwalnianie pamieci z tablicy wskaznikow odpowiadajacej kolumna
  return 0;
}

void wyswietl_tablice(){
  cout<<endl;
  for(int wie=0;wie<wiersze;wie++){
    for(int kol=0;kol<kolumny;kol++){
      cout<<tablica[kol][wie]<<" "; // wyswietlanie elementow
    }
    cout<<endl; // przewijanie wierszy
  }
  cout<<endl;
}

void wypelnij_tablice_z_terminalu(){
  cout<<endl;
  for(int wie=0;wie<wiersze;wie++){
    for(int kol=0;kol<kolumny;kol++){
      cout<<"Podaj wartosc elementu["<<kol<<"]["<<wie<<"]: ";
      cin>>tablica[kol][wie];
    }
  }
  cout<<endl;
}

void wypelnij_tablice_l_losowymi(){
  int zakres=1;
  srand(time(NULL)); // ustalenie punktu poczatkowego w danym monencie czasu procesora
  cout<<endl<<"podaj zakres liczby: ";
  cin>>zakres;
  for(int wie=0;wie<wiersze;wie++){
    for(int kol=0;kol<kolumny;kol++){
      tablica[kol][wie]=std::rand()%(zakres+1); // przypisywanie losowej wartosci do zmiennej
    }
  }
  cout<<endl;
}

int wartosc_max_tablicy(){
  int max=-200;
  for(int wie=0;wie<wiersze;wie++){
    for(int kol=0;kol<kolumny;kol++){
      if(tablica[kol][wie]>max)max=tablica[kol][wie]; // przypisywanie najwiekszej wartosci do zmiennej
    }
  }
  return max;
}
